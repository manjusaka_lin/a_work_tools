adb push 0-9-1000.mp3 /sdcard/
echo "adb push yeban.mp3 /sdcard/"
adb root;
CMD_PRE="adb shell"
function check_if_activity_showup()
{
	echo "want act: " $1
	re=$(dumpsys SurfaceFlinger | grep $1 )
	#echo "check result:" $re
	if [ -z "$re" ];then
		echo "lhs=>" $1 "not found !!!"
		return 0
	else
		echo "lhs=>" $1 "found !!!"
		return 1
	fi
}


function wait_act_showup()
{
	let w_cnt=0;
	check_if_activity_showup $1
	while (( $? == 0 ));do
		sleep 2
		let w_cnt=$w_cnt+1;
		if [[ $w_cnt > 4 ]];then
			return 0
		fi
		echo $w_cnt
		check_if_activity_showup $1  ##must put this at last line for $? is the last process line.
	done
}
function do_unlock_swap()
{
	sleep 2
	$CMD_PRE input keyevent 3
	$CMD_PRE input keyevent 3
	check_if_activity_showup "com.android.launcher3"
	if [ $? == 1 ];then
		echo "already unlock"
		$CMD_PRE input swipe 336 530 300 180
		$CMD_PRE input keyevent 3
	else
		echo "lhs=>going to unlock"
		$CMD_PRE input swipe 336 530 300 180
		$CMD_PRE input keyevent 3
	fi
	$CMD_PRE input keyevent 3
	wait_act_showup com.android.launcher3
}
while(true)
do
    echo "begin custom test ..."
    #echo "monkeytesting" > /sys/class/android_usb/android0/iSerial
    #echo $RANDOM > /sys/class/android_usb/android0/iSerial
    br=$(cat /sys/class/leds/lcd-backlight/brightness)
    echo $br
    if [ $br -eq "0" ]
    then
    	echo "lcd is off, power it up"
    	$CMD_PRE input keyevent 26
    fi
    
  #for sprd platform
    br=$(cat /sys/class/backlight/sprd_backlight/brightness)
    echo $br
    if [ $br -eq "0" ]
    then
    	echo "lcd is off, power it up"
    	$CMD_PRE input keyevent 26
    fi


    #echo $RANDOM > /sys/class/android_usb/android0/iSerial
    $CMD_PRE input keyevent 4
    sleep 1
    $CMD_PRE do_unlock_swap
    sleep 1

    $CMD_PRE input swipe 300 500 300 150
    sleep 1
    #$CMD_PRE am start -n  com.android.gallery3d/.app.MovieActivity -d file:///sdcard/yeban.mp3
    $CMD_PRE am start -n com.android.music/com.android.music.MusicBrowserActivity
    sleep 3
    #$CMD_PRE am start -n com.android.music/com.android.music.MediaPlaybackActivity -d file:///sdcard/yeban.mp3
    $CMD_PRE input keyevent 4
    $CMD_PRE input keyevent 4
    $CMD_PRE input keyevent 4
    #$CMD_PRE am start -n  com.android.gallery3d/.app.MovieActivity -d file:///sdcard/yeban.mp3
    $CMD_PRE am start -n com.android.music/com.android.music.MediaPlaybackActivity -d file:///sdcard/0-9-1000.mp3

    sleep 10
done 
