sudo mkdir /media/misc_data
sudo mkdir /media/4Tdata
sudo mkdir /media/o4Tdata
sudo mkdir /media/eightdata
sudo mkdir /media/ssd300

sudo chown manjusaka:manjusaka /media/misc_data
sudo chown manjusaka:manjusaka /media/4Tdata
sudo chown manjusaka:manjusaka /media/o4Tdata
sudo chown manjusaka:manjusaka /media/eightdata
sudo chown manjusaka:manjusaka /media/ssd300

sudo echo "
UUID=8ce4d713-6528-429c-a3b9-65a4efa1039c   /media/misc_data  ext4    rw,defaults  0   2
UUID=9943659b-1a3d-4791-ac15-a28cabf2bad9   /media/4Tdata  ext4    rw,defaults  0   2
UUID=23579ab2-053f-4e77-8836-4043398cb85a   /media/o4Tdata  ext4    rw,defaults  0   2
#UUID=e937eb38-2b04-45df-af53-f72ed94f4018   /media/2Tdata  ext4    rw,defaults  0   2
UUID=2fa53b98-4bcf-40e0-9b02-d24af3c27eb1   /media/eightdata  ext4    rw,defaults  0   2
#UUID=387756bf-ae79-4421-9951-c9a2eeb7699e  /media/ssd300  ext4    rw,defaults  0   2
UUID=b556a019-43d0-4fe3-a2c3-85a1a51d6948  /media/ssd300  ext4    rw,defaults  0   2
" >> /etc/fstab
