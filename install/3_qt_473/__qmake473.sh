mkdir -p /home/$(whoami)/tmp/pyqt4
tar zxvf qt-everywhere-opensource-src-4.7.3.tar.gz -C /home/$(whoami)/tmp/pyqt4
cd /home/$(whoami)/tmp/pyqt4/qt-everywhere-opensource-src-4.7.3
./configure -xmlpatterns -webkit
make
sudo make install

