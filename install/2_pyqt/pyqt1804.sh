#https://packages.ubuntu.com/focal/amd64/qt5-qmake/download
echo "https://packages.ubuntu.com/focal/amd64/qt5-qmake/download"
sudo add-apt-repository ppa:rock-core/qt4
sudo apt update
(echo "Y") |sudo apt install libqt4-declarative libqtcore4 libqtgui4 libqtwebkit4
(echo "Y") |sudo apt install qt4*
(echo "Y") |sudo apt install libqt4*

pip install python-jenkins
mkdir -p /home/$(whoami)/tmp/sip
tar zxvf sip-4.19.2.tar.gz -C /home/$(whoami)/tmp/sip
cd /home/$(whoami)/tmp/sip/sip-4.19.2
python configure.py
sudo make install
cd -

pip install virtualenv

(echo "Y") | sudo apt-get install qt4-dev-tools qt4-doc qt4-qtconfig qt4-demos qt4-designer
(echo "Y") | sudo apt-get install libqwt5-qt4 libqwt5-qt4-dev

mkdir -p /home/$(whoami)/tmp/pyqt4
tar zxvf PyQt4_gpl_x11-4.12.1.tar.gz -C /home/$(whoami)/tmp/pyqt4
cd /home/$(whoami)/tmp/pyqt4/PyQt4_gpl_x11-4.12.1
python configure.py
make
sudo make install

