sudo cp jdk-6u37-linux-x64.bin /opt/
cd /opt/
sudo chmod 777 jdk-6u37-linux-x64.bin
sudo ./jdk-6u37-linux-x64.bin
cd -
#sudo sh javacfg.sh

sudo cp java-7-openjdk-amd64.tar /opt/
cd /opt/
sudo tar xvzf java-7-openjdk-amd64.tar
cd -

CURJ="jdk1.6.0_37"
cd "/opt/$CURJ/bin"
for file in $(ls)
do 
	sudo update-alternatives --install /usr/bin/$file $file /opt/$CURJ/bin/$file 1
done


CURJ="java-7-openjdk-amd64"
cd "/opt/$CURJ/bin"
for file in $(ls)
do 
	sudo update-alternatives --install /usr/bin/$file $file /opt/$CURJ/bin/$file 1
done


