#1.首先必须先安装 node 环境
sudo apt install nodejs
sudo apt install npm

#2.安装完成之后查看一下 nodejs 和 npm 的版本
#$ node -v
#$ npm -v
#
#3.npm 全局安装利器 commitizen
sudo npm install -g commitizen

#4.先全局下载规范模板文件
sudo npm install -g cz-conventional-changelog
echo '{ "path": "cz-conventional-changelog" }' > ~/.czrc

#5.使用 git commit 的地方更换成 git cz 就能显示出 commit 的交互界面
