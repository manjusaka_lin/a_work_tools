#第五种方法
#从版本3.15.4开始，Nautilus不再加载accel文件(Source)。

#幸运的是，为了得到你想要的东西，有一个更好的方法。可以找到长解释/有用资源here以及here。简而言之：

#在~/.local/share/nautilus/scripts文件夹中创建一个名为Terminal(是的，没有扩展名)的脚本，其中包含以下内容：

## !/bin/sh
#gnome-terminal
#使其可执行，然后关闭任何Nautilus实例：

#$ chmod +x Terminal
#$ nautilus -q
#创建(或编辑)添加以下行的~/.config/nautilus/scripts-accels文件：

#F12 Terminal
#; Commented lines must have a space after the semicolon
#; Examples of other key combinations:
#; <Control>F12 Terminal
#; <Alt>F12 Terminal
#; <Shift>F12 Terminal
#测试一下！打开Nautilus，右键单击，然后选择Scripts>终奌站。或者，使用刚刚配置的键盘快捷键:)

#注意：在Ubuntu 18.04上测试过。

echo '''
#!/bin/sh
#gnome-terminal
terminator $pwd
''' > Terminal;
cp Terminal ~/.local/share/nautilus/scripts
chmod 777 ~/.local/share/nautilus/scripts/Terminal
echo '''<Alt>c Terminal
; Commented lines must have a space after the semicolon
; Examples of other key combinations:
; <Control>F12 Terminal
; <Alt>F12 Terminal
; <Shift>F12 Terminal
''' > scripts-accels;
cp scripts-accels ~/.config/nautilus/;
nautilus -q
