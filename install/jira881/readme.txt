
一、 说明
1.1 素材

本文采用素材如下：
Docker镜像 Github链接(https://github.com/cptactionhank)
破解工具 Gitee链接(https://gitee.com/pengzhile/atlassian-agent) (https://zhile.io/2018/12/20/atlassian-license-crack.html)

采用以上工具，理论上可以破解几乎全部版本。

本地下载地址：https://files.cnblogs.com/files/sanduzxcvbnm/atlassian-agent-v1.2.3.zip

1.2 数据库

如果是选择外部数据库，大家可以按照这样创建：

# 创建jira数据库及用户
CREATE DATABASE jiradb CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;
grant all on jiradb.* to 'jirauser'@'%' identified by 'tVxxxb6n';

二、 安装 JIRA(8.8.1)

JIRA 是一个缺陷跟踪管理系统，为针对缺陷管理、任务追踪和项目管理的商业性应用软件，开发者是澳大利亚的Atlassian。JIRA这个名字并不是一个缩写，而是截取自“Gojira”，日文的哥斯拉发音。 官网
2.1 制作Docker破解容器

编写Dockerfile文件：

FROM cptactionhank/atlassian-jira-software:latest

USER root

# 将代理破解包加入容器
COPY "atlassian-agent.jar" /opt/atlassian/jira/

# 设置启动加载代理包
RUN echo 'export CATALINA_OPTS="-javaagent:/opt/atlassian/jira/atlassian-agent.jar ${CATALINA_OPTS}"' >> /opt/atlassian/jira/bin/setenv.sh
2.2 下载破解文件

在gitee 中下载编译好的即可，放置在Dockerfile同目录下

JIRA
--Dockerfile
--atlassian-agent.jar
2.3 构建镜像

docker build -t jira:latest .
结果如下：

Sending build context to Docker daemon  2.141MB
Step 1/4 : FROM cptactionhank/atlassian-jira-software:latest
 ---> c51100467795
Step 2/4 : USER root
 ---> Running in 3f9cea0602c7
Removing intermediate container 3f9cea0602c7
 ---> 4b9e20ba43cf
Step 3/4 : COPY "atlassian-agent.jar" /opt/atlassian/jira/
 ---> 61155470b50a
Step 4/4 : RUN echo 'export CATALINA_OPTS="-javaagent:/opt/atlassian/jira/atlassian-agent.jar ${CATALINA_OPTS}"' >> /opt/atlassian/jira/bin/setenv.sh
 ---> Running in 5aed1ac41ab7
Removing intermediate container 5aed1ac41ab7
 ---> 33d0b86f8262
Successfully built 33d0b86f8262
Successfully tagged jira:latest
2.4 启动容器

# 提前创建/home/jira目录
docker run -d --name jira\
  --restart always \
  -p 18009:8080 \
  -e TZ="Asia/Shanghai" \
  -m 4096M \
  -v /home/jira:/var/atlassian/jira \
  jira:latest
2.5 访问 jira

访问 IP:18009，选择语言并选择手动配置


演示使用内置数据库（生产环境需配置独立数据库）：


设置属性


2.6 破解



复制服务器ID: BRQE-TEN6-TLYV-KFMI
在本地存放atlassian-agent.jar的目录下执行命令，生成许可证：
需替换邮箱（test@test.com）、名称（BAT）、访问地址（http://192.168.0.89）、服务器ID（BY9B-GWD1-1C78-K2DE）为你的信息
java -jar atlassian-agent.jar \
  -d -m test@test.com -n BAT \
  -p jira -o http://192.168.0.89 \
  -s BY9B-GWD1-1C78-K2DE
例如我的信息如下，生成许可证：

java -jar atlassian-agent.jar \
  -d -m wangzan18@126.com -n BAT \
  -p jira -o http://jira.wzlinux.com \
  -s BRQE-TEN6-TLYV-KFMI

====================================================
=======        Atlassian Crack Agent         =======
=======           https://zhile.io           =======
=======          QQ Group: 30347511          =======
====================================================

Your license code(Don't copy this line!!!): 

AAABoQ0ODAoPeJx9ktFPqzAUxt/5K0h8LpaazbmE5CrUhAhMB5rcx46dbTWskNMynX+9Hcy467wkv
LSn5/vO+X1c5K1yU7F3fer6bEpHU8rcMC9cRhl1XiUKr8F62ZbGOxyIrlfmTSB4ojRyB4HBFpxUS
GVACVUCf28k7iNhIGB0ckPotf2GdLJ2uwCcrZ41oA6I37+1AiIEK4q9Qw64A4yj4G7+xEnBszEpk
r8v5OE+jZ1ElqA02GoSRznPSOKPJjd0cjWi7HrsO2GtjB2X2zGr4E2o9YdQ/uSPz8ZeWW97w9N1+
E5UrTCyVsFKVBqcxxbLjdBwXItRQkfEp1/Gxb6BTGwhCGdpyudhfJs4awRQm7ppAP/R7swG+rr6G
ZNjw+9wz16feg9YRaBLlE2357Oq5FYaWLpV3+Au9u7GmEZPLy8/NrICT9ZDMeZG4CGtHtiReOd4d
1s4M1wLJXXP9CBrVTsxyx/trVrrLgobXvBbgAhd60/+ebv4XuEML1/KfrcsidO44NHQ/D9/uFOG3
XWDUsN57b9eR44vNpNDgX0C7gEsJDAsAhQGL/A02nteG056fiVCh12XIgz+KwIUG3z2e35ugE7Pc
N6ZMj+Aum9LTK8=X02k4
将生成的许可证复制到页面，完成破解。

