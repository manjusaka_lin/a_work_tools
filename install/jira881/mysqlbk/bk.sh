#! /bin/bash

echo "########################################"
TIMESTAMP=$(date +"%F" -d "-1day")
#备份路径
BACKUP_DIR="./"
MYSQL_USER="jira"
MYSQL_HOST="192.168.6.253"
MYSQL=/usr/bin/mysql
MYSQL_PASSWORD="jira"
MYSQLDUMP=/usr/bin/mysqldump
databases=`$MYSQL -h$MYSQL_HOST -u$MYSQL_USER -p$MYSQL_PASSWORD -N -e "SHOW DATABASES;" | grep -Ev "(information_schema|performance_schema|mysql|test)"`
echo $databases

[ ! -d "$BACKUP_DIR" ] && mkdir -p "$BACKUP_DIR"

for db in $databases; do
	$MYSQLDUMP -h$MYSQL_HOST --force --opt --single-transaction --master-data=2 -F -u$MYSQL_USER -p$MYSQL_PASSWORD --databases $db > "$BACKUP_DIR"/"$db".sql
done
